const uri = "api/map";
var vectorSource = new ol.source.Vector();
var infospace = document.getElementById("infospace");
var markerlist = document.getElementById("markerlist");
var button1 = document.getElementById("button1");
var currentFeature;

var lat1 = 10;
var lon1 = 10;

$(document).ready(function () {
    getData();
    map.addEventListener("click", markerCreateNew);
    button1.addEventListener("click", function () {
        markerEdit(2, lat1, lon1);
        lat1 += 10;
        lon1 += 10;
    });
});

function getData() {
    // downloads data from the database
    allinfo = "";
    $.ajax({
        type: "GET",
        url: uri,
        cache: false,
        success: function (data) {

            $.each(data, function (key, item) {
                markerAddToVector(item.lon, item.lat, item.useragent, item.id);
                allinfo += item.id + ":<br>"
                    + item.lon + " " + item.lat + "<br>"
                    + item.useragent + "<br>";
            });

            markerlist.innerHTML = allinfo + "<br>";
        }
    });
}

// ctrl detection
var ctrlKeyCode = 17;
var ctrlIsPressed = false;

function keydownFunction(event) {
    if (event.which == "17")
        ctrlIsPressed = true;
}
function keyupFunction() {
    ctrlIsPressed = false;
}

//create the icon style
var iconStyle = new ol.style.Style({
    image: new ol.style.Icon(/** @type {olx.style.IconOptions} */({
        anchor: [0.5, 46],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        opacity: 0.95,
        scale: 0.1,
        src: 'res/PackageIcon.png'
    }))
});
var mapLayer = new ol.layer.Tile({
    source: new ol.source.OSM()
});
var vectorLayer = new ol.layer.Vector({
    source: vectorSource,
    style: iconStyle
});
var map = new ol.Map({
    target: 'map',
    layers: [mapLayer, vectorLayer],
    view: new ol.View({
        center: ol.proj.fromLonLat([21.00698, 52.22226]),
        zoom: 15
    })
});

function markerCreateNew(evt) {
    // get coords from the map
    var coord = ol.proj.transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326');
    var lon = coord[0];
    var lat = coord[1];
    var useragent = navigator.userAgent;

    markerAddToDatabase(lon, lat, useragent);
}
function markerAddToVector(lon, lat, useragent, id) {
    // basically adds the marker to memory
    // and also shows it on the map
    var iconFeature = new ol.Feature({
        geometry: new ol.geom.Point(ol.proj.fromLonLat([lon, lat])),
        lon: lon,
        lat: lat,
        useragent: useragent,
        id: id
    });
    vectorSource.addFeature(iconFeature);
}
function markerAddToDatabase(lon, lat, useragent) {
    const item = {
        lon: lon,
        lat: lat,
        useragent: useragent
    };

    $.ajax({
        type: "POST",
        accepts: "application/json",
        url: uri,
        contentType: "application/json",
        data: JSON.stringify(item),
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Something went wrong!");
        },
        success: function (result) {
            getData();
        }
    });
}

function mapOnMarkerClick(evt) {
    ctrlIsPressed ?
        mapDeleteMarker(evt) : mapMarkerInfo(evt);
}
function mapDeleteMarker(evt) {
    // delete
    map.forEachFeatureAtPixel(evt.pixel, function (feature, layer) {
        deleteItem(feature.values_.id);
        vectorSource.removeFeature(feature);
        infospace.innerHTML = "";
    })
}
function mapMarkerInfo(evt) {
    map.forEachFeatureAtPixel(evt.pixel, function (feature, layer) {
        currentFeature = feature;
        var values = feature.values_;
        if (values.lon != "") {
            infospace.innerHTML =
                `coordinates: <br> ${values.lon}, ${values.lat} <br>` +
                "<br>" +
                `useragent: <br> ${values.useragent} <br>` +
                "<br>" +
                `id: <br> ${values.id} <br>`
            "<br>";
            return;
        }
    });
}

map.on('pointermove', function (evt) {
    if (map.hasFeatureAtPixel(evt.pixel)) {
        // change cursor when pointing at package
        map.getTargetElement().style.cursor = 'pointer';

        map.removeEventListener("click", markerCreateNew);
        map.addEventListener("click", mapOnMarkerClick);
    }
    else {
        map.getTargetElement().style.cursor = '';

        map.removeEventListener("click", mapOnMarkerClick);
        map.addEventListener("click", markerCreateNew);
    }
});

function deleteItem(id) {
    $.ajax({
        url: uri + "/" + id,
        type: "DELETE",
        success: function (result) {
            getData();
        }
    });
}

function markerEdit(id, lon, lat) {
    var useragent = navigator.userAgent;
    const item = {
        id: id,
        lon: lon,
        lat: lat,
        useragent: useragent
    };

    $.ajax({
        url: uri + "/" + id,
        type: "PUT",
        accepts: "application/json",
        contentType: "application/json",
        data: JSON.stringify(item),
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Something went wrong!");
        },
        success: function (result) {
            getData();
        }
    });

    //TODO: remove the marker from vectorSource
}
