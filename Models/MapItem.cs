﻿namespace MapApi.Models
{
    public class MapItem
    {
        public double lon { get; set; }
        public double lat { get; set; }
        public long timestamp { get; set; }
        public string browser { get; set; }
        public string platform { get; set; }
        public string useragent { get; set; }
        public string hash { get; set; }
        public long id { get; set; }
    }
}
