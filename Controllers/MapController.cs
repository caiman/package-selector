﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using MapApi.Models;

namespace MapApi.Controllers
{
    [Route("api/map")]
    [ApiController]
    public class MapController : ControllerBase
    {
        private readonly MapContext _context;

        public MapController(MapContext context)
        {
            _context = context;

            if (_context.MapItems.Count() == 0)
            {
                // Create a new MapItem if collection is empty,
                // which means you can't delete all MapItems.
                _context.MapItems.Add(new MapItem {
                    lon= 0,
                    lat= 0,
                    id= 0,
                    timestamp= 0,
                    browser= "test",
                    platform= "test",
                    useragent= "test",
                    hash= "test"
                });
                _context.SaveChanges();
            }
        }

        [HttpGet]
        public ActionResult<List<MapItem>> GetAll()
        {
            return _context.MapItems.ToList();
        }


        [HttpGet("{id}", Name = "GetMap")]
        public ActionResult<MapItem> GetById(long id)
        {
            var item = _context.MapItems.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpPost]
        public IActionResult Create(MapItem item)
        {
            _context.MapItems.Add(item);
            _context.SaveChanges();

            return CreatedAtRoute("GetMap", new { id = item.id }, item);
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, MapItem item)
        {
            var marker = _context.MapItems.Find(id);
            if (marker == null)
            {
                return NotFound();
            }

            marker.lon              = item.lon;
            marker.lat              = item.lat;
            marker.id               = item.id;
            marker.timestamp        = item.timestamp;
            marker.browser          = item.browser;
            marker.platform         = item.platform;
            marker.useragent        = item.useragent;
            marker.hash             = item.hash;

            _context.MapItems.Update(marker);
            _context.SaveChanges();
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var marker = _context.MapItems.Find(id);
            if (marker == null)
            {
                return NotFound();
            }

            _context.MapItems.Remove(marker);
            _context.SaveChanges();
            return NoContent();
        }

    }
}
